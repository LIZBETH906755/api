//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3002;

  var bodyParser = require('body-parser');
  app.use(bodyParser.json()); // support json encoded bodies
  app.use(function(req, res, next){
    res.header ("Access-Control-Allow-Origin", "*");
    res.header ("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next ();
  }); // support encoded bodies

var requestjson =require ('request-json');

var path = require('path');

var urlMovimientos = "https://api.mlab.com/api/1/databases/bdbanca3m906755/collections/movimientos?apiKey=jBSfB3UB-Z5ApUn_m_qAtEx6lwMGZXzt";
var urlUsuarios = "https://api.mlab.com/api/1/databases/bdbanca3m906755/collections/usuarios?apiKey=jBSfB3UB-Z5ApUn_m_qAtEx6lwMGZXzt";

var clienteMlab =requestjson.createClient(urlMovimientos);
var usuariosMlab =requestjson.createClient(urlUsuarios);
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname,'index.html'));
});

/*app.get("/movimientos/:idcliente", function(req,res){

var idcliente = req.params.idcliente
//var query = '&q={"idCustomer":'+idcliente+'}&f={"_id":1}'
var query = '&q={"idcliente":'+idcliente+'}'
var buscaCliente=urlMovimientos+query
var movimientosMlabFind=requestjson.createClient(buscaCliente);

movimientosMlabFind.get('', function(err, resM, body){
if(err){
  console.log(body);
  }
  else{
    res.send(body);
    console.log ('movimientos por cliente');
  }
});
});*/

app.get("/movimientos/:cdcuenta", function(req,res){

var idcuenta = req.params.cdcuenta
//var query = '&q={"idCustomer":'+idcliente+'}&f={"_id":1}'
var query = '&q={"cdcuenta":'+idcuenta+'}'
var buscaCuenta=urlMovimientos+query
var movimientosMlabFind=requestjson.createClient(buscaCuenta);

movimientosMlabFind.get('', function(err, resM, body){
if(err){
  console.log(body);
  }
  else{
    res.send(body);
    console.log ('entro a movimientos por cdcuenta');
    body.forEach(function(elemento) {
    console.log('mov por cuenta', elemento.cdcuenta);
  });
  }
});
});

app.get('/movimientos', function(req, res){

  clienteMlab.get ('', function (err, resM, body){
    if (err){
      console.log (err);
    } else {
      res.send (body);
      console.log ('todos');
    }
  });
});

app.get('/usuarios', function(req, res){

  usuariosMlab.get ('', function (err, resM, body){
    if (err){
      console.log (err);
    } else {
      res.send (body);
      console.log ('todos');
    }
  });
});

// Busca a por id cuenta en la collections de usuarios
app.get("/usuarios/:cdcuenta", function(req,res){
var idusuario = req.params.cdcuenta
//var query = '&q={"idCustomer":'+idcliente+'}&f={"_id":1}'
var query = '&q={"cdcuenta":'+idusuario+'}'
var buscaUsuarios=urlUsuarios+query
var usuariosMlabFind=requestjson.createClient(buscaUsuarios);

usuariosMlabFind.get('', function(err, resM, body){
if(err){
  console.log(body);
  }
  else{
    res.send(body);
    body.forEach(function(elemento) {
    var cuentaFind =  elemento.cdcuenta
    console.log('usuarios por cuenta', elemento.cdcuenta, +cuentaFind);
    console.log (elemento.cdcuenta);
  });
  }
});
});



app.post('/', function(req, res){
  res.send('Su petición ha sido recibida cambiada');
});

// parameters sent with
app.post('/movimientos', function(req, res) {
  clienteMlab.post('', req.body, function (err, resM, req){
    if (err){
      console.log (err);
    } else {
      res.send (req);
        console.log ('post a movimientos');
    }
  });
});

// parameters sent with
app.post('/usuarios', function(req, res) {
  usuariosMlab.post('', req.body, function (err, resM, req){
    if (err){
      console.log (err);
    } else {
      res.send (req);
        console.log ('post a usuarios');
    }
  });
});
